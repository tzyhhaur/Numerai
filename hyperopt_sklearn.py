from common import *

import sys
sys.path.insert(0, '../myPythonScripts')

from myHyperoptSklearn import *
from myPCA import *

def main():
    data = load_data(split_val=True)
    PCAmean2000 = load_pca()

    X_train = data['X_train']
    X_test = data['X_test']
    X_val = data['X_val']

    X_train = pca_transform(X_train, PCAmean2000)
    X_test = pca_transform(X_test, PCAmean2000)
    X_val = pca_transform(X_val, PCAmean2000)

    data.update({
        'X_train': X_train,
        'X_test': X_test,
        'X_val': X_val
    })
    optimize_all(data)

if __name__ == '__main__':
    main()
