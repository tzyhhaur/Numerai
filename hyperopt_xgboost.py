from common import *

from hyperopt import hp
import sys
sys.path.insert(0, '../myPythonScripts')

from myXgboost import *
from myPCA import *

def main():
    data = load_data(split_val=True)
    X_train = data['X_train']
    X_val = data['X_val']
    X_test = data['X_test']

    PCAmean = np.load('intermediate_data/PCAmean_20000.npy')
    X_train = pca_transform(X_train,PCAmean)
    X_val = pca_transform(X_val,PCAmean)
    X_test = pca_transform(X_test,PCAmean)

    data.update({
        'X_train': X_train,
        'X_val': X_val,
        'X_test': X_test
    })

    space = {
        'n_estimators': hp.quniform('n_estimators', 100, 1000, 1),
        'eta': hp.quniform('eta', 0.01, 0.5, 0.02),
        # A problem with max_depth casted to float instead of int with
        # the hp.quniform method.
        'max_depth':  hp.choice('max_depth', np.arange(1, 50, dtype=int)),
        'min_child_weight': hp.quniform('min_child_weight', 1, 10, 1),
        'subsample': hp.quniform('subsample', 0.5, 1, 0.05),
        'gamma': hp.quniform('gamma', 0.2, 1, 0.05),
        'colsample_bytree': hp.quniform('colsample_bytree', 0.2, 1, 0.05),
        'eval_metric': 'mlogloss',
        'objective': 'binary:logistic',

        # Increase this number if you have more cores. Otherwise, remove it and it will default
        # to the maxium number.
        'nthread': 10,
        'booster': 'gbtree',
        'tree_method': 'exact',
        'silent': 1,
    }
    best_scores = optimize_xgboost(data, space, 20)
    np.save('parameters_tuning/hyperopt_xgboost_best_scores.npy',best_scores)

if __name__ == '__main__':
    main()
