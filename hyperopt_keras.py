from common import *
from hyperopt import Trials, STATUS_OK, tpe

import sys
sys.path.insert(0, '../myPythonScripts')

from myKerasHyperopt import *
from keras.utils.np_utils import to_categorical
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from myPCA import *


def main():
    data = load_data(split_val=True)
    X_train = data['X_train']
    X_val = data['X_val']
    X_test = data['X_test']

    PCAmean = np.load('intermediate_data/PCAmean_20000.npy')
    X_train = pca_transform(X_train,PCAmean)
    X_val = pca_transform(X_val,PCAmean)
    X_test = pca_transform(X_test,PCAmean)

    data.update({
        'X_train': X_train,
        'X_val': X_val,
        'X_test': X_test
    })

    space = {'choice': hp.choice('num_layers',
                    [ {'layers':'two', },
                    {'layers':'three',
                    'units3': hp.quniform('units3', 200,1024,50),
                    'dropout3': hp.uniform('dropout3', .25,.75)}
                    ]),

            'units1': hp.quniform('units1', 50,1000,50),
            'units2': hp.quniform('units2', 50,1000,50),

            'dropout1': hp.uniform('dropout1', .25,.75),
            'dropout2': hp.uniform('dropout2',  .25,.75),

            'batch_size' : hp.quniform('batch_size', 15,100,5),

            #'nb_epochs' : hp.choice('max_depth', np.arange(10, 100, dtype=int)),
            'nb_epochs' : 30,
            'optimizer': hp.choice('optimizer',['adadelta','adam','rmsprop']),
            #'optimizer': hp.choice('optimizer',['adam']),
            'activation': hp.choice('activation',['relu','softmax','tanh','sigmoid'])
            #'activation': 'relu'
        }

    for i in range(20):
        best_params = optimize_keras(data, space, 1)
        np.save('parameters_tuning/hyperopt_keras_parallel_linode_run{}.npy'.format(i+1),best_params)

if __name__ == '__main__':
    main()
    print('Finish running hyperopt_keras');
