from common import *
from hyperopt import Trials, STATUS_OK, tpe

import sys
sys.path.insert(0, '../myPythonScripts')

from myKerasHyperopt import *
from myPCA import *
from keras.utils.np_utils import to_categorical

def main():
    data = load_data(split_val=True)
    X_train = data['X_train']
    X_val = data['X_val']
    X_test = data['X_test']

    PCAmean = np.load('intermediate_data/PCAmean_20000.npy')
    X_train = pca_transform(X_train,PCAmean)
    X_val = pca_transform(X_val,PCAmean)
    X_test = pca_transform(X_test,PCAmean)

    data.update({
        'X_train': X_train,
        'X_val': X_val,
        'X_test': X_test
    })
    optimize_keras(data)

if __name__ == '__main__':
    main()
