import sys
sys.path.insert(0, '../myPythonScripts')

from myPCA import *
from common import *

def main():
    data=load_data()
    #pca_study(data, 100, 0.7, threshold=1e-8)
    PCAmean, PCAstd = estimate_pca_components(data,21,20000,0.6)
    np.save("PCAmean_20000.npy",PCAmean)
    np.save("PCAstd_20000.npy",PCAstd)

if __name__ == '__main__':
    main()
