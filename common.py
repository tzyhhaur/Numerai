import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split


def load_pca():
    PCAmean = np.load('intermediate_data/PCAmean_20000.npy')
    return PCAmean


def load_data(split_val=True):
    train_raw = np.array(pd.read_csv('latest_data/numerai_training_data.csv'))
    tournament_raw = np.array(pd.read_csv('latest_data/numerai_tournament_data.csv'))

    X_train = train_raw[:,:-1]
    Y_train = train_raw[:,-1].reshape(X_train.shape[0],1)
    Y_train = Y_train.astype(int)

    X_test = tournament_raw[:,1:]
    ID_test = tournament_raw[:,0].reshape(tournament_raw.shape[0],1)


    if split_val:
        X_train, X_val, Y_train, Y_val = train_test_split(X_train, Y_train, test_size=0.3, random_state=1234)
        Y_train = Y_train.reshape(X_train.shape[0],1)
        Y_val = Y_val.reshape(X_val.shape[0],1)

        data = {
            'X_train':X_train,
            'Y_train': Y_train,
            'X_val': X_val,
            'Y_val': Y_val,
            'X_test': X_test,
            'ID_test': ID_test
        }
    else:
        data = {
            'X_train':X_train,
            'Y_train': Y_train,
            'X_test': X_test,
            'ID_test': ID_test
        }


    return data
