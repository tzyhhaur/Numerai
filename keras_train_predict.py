from common import *
from hyperopt import Trials, STATUS_OK, tpe

import sys
sys.path.insert(0, '../myPythonScripts')

from myKerasHyperopt import *
from keras.utils.np_utils import to_categorical
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import glob


def predict(params,submission_path = 0):
    X_train, Y_train, X_test = load_data()

    X_train = addAverageDonation(X_train)
    X_test = addAverageDonation(X_test)

    X_train = addMomentum(X_train)
    X_test = addMomentum(X_test)

    #X_train = addLastFirstRatio(X_train)
    #X_test = addLastFirstRatio(X_test)

    scaler = StandardScaler().fit(X_train)
    X_train = scaler.transform(X_train)
    X_test = scaler.transform(X_test)

    X_train, X_val, Y_train, Y_val = train_test_split(X_train, Y_train, test_size=0.2, random_state=1234)

    Y_train = Y_train.reshape(X_train.shape[0],1)
    Y_val = Y_val.reshape(X_val.shape[0],1)

    data = { 'X_train':X_train, 'Y_train': Y_train, 'X_val': X_val, 'Y_val': Y_val, 'X_test':X_test }

    #params = {'activation': 2, 'num_layers': 1, 'dropout2': 0.6570677961472692, 'optimizer': 2, 'units2': 800.0, 'units3': 250.0, 'batch_size': 57.0, 'dropout3': 0.6254265630083905, 'units1': 750.0, 'dropout1': 0.4135896268959048}

    if params['num_layers'] == 0:
        params.update({
            'choice': {
                'layers': 'two'
            }
        })
    else:
        params.update({
            'choice': {
                'layers': 'three',
                'units3': params['units3'],
                'dropout3': params['dropout3']
            }
        })

    optimizer_choice = ['adadelta','adam','rmsprop']
    params.update({
        'optimizer': optimizer_choice[params['optimizer']]
    })

    activation_choice = ['relu','softmax','tanh','sigmoid']
    params.update({
        'activation': activation_choice[params['activation']]
    })
    params.update({
        'nb_epochs' :  100,
    })

    output = f_nn(params, data)
    if submission_path == 0:
        return output['loss']
    else :
        model = output['model']
        print("Score of the model is {}".format(output['loss']))
        Y_test = model.predict_proba(X_test)
        create_submission(Y_test,submission_path)

def loop_predict():
    all_parameters = glob.glob('parameters_tuning/*')

    best_score = 999
    best_score_path =1
    for parameters_path in all_parameters:
        params = np.load(parameters_path)
        params = params.item(0)
        loss_score = predict(params)
        if best_score > loss_score:
            best_score = loss_score
            best_score_path = parameters_path

    print("Best score: {} from {}".format(best_score,best_score_path))

if __name__ == '__main__':
    #params = np.load('parameters_tuning/hyperopt_keras_parallel4_run5.npy')
    #params = params.item(0)
    params = {'activation': 2, 'num_layers': 1, 'dropout2': 0.6570677961472692, 'optimizer': 2, 'units2': 800.0, 'units3': 250.0, 'batch_size': 57.0, 'dropout3': 0.6254265630083905, 'units1': 750.0, 'dropout1': 0.4135896268959048}
    predict(params,'data/BloodDonationSubmission12Feb.csv')
